package kraig

import cats.free.Free
import cats.instances.all._
import Config._

class JsonConfigTest extends test.TestBase {
  behavior of "JsonConfig interpreter"

  def test[A](json: String, prog: Free[Config, A]): Option[A] =
    prog.foldMap(JsonConfig.interpret(json))

  it should "interpret a string" in {
    val prog = for {
      prop <- getString("prop")
    } yield prop

    forAll { (string: String) =>
      val json = s"""{"prop": "$string"}"""
      test(json, prog) shouldBe Some(string)
    }
  }

  it should "interpret an int" in {
    val prog = for {
      i <- getInt("prop")
    } yield i

    forAll { (i: Int) =>
      val json = s"""{"prop": $i}"""
      test(json, prog) shouldBe Some(i)
    }
  }

  it should "interpret an optional string and int(using null)" in {
    val prog = for {
      i <- getOptionalInt("int")
      s <- getOptionalString("str")
    } yield (i, s)

    forAll { (str: Option[String], i: Option[Int]) =>
      // This hurt to type but it is ok because we want the json to have a null property if the properties aren't there
      val strJson = str
        .map('"' + _ + '"')
        .getOrElse(null)
      val iJson = i.getOrElse(null)

      val json = s"""{"str": $strJson, "int": $iJson}"""
      test(json, prog) shouldBe Some((i, str))
    }
  }

  it should "interpret an optional string and int(when they are absent)" in {
    val prog = for {
      i <- getOptionalInt("int")
      s <- getOptionalString("str")
    } yield (i, s)

    forAll { (str: Option[String], i: Option[Int]) =>
      val strJson = str.map(x => s""""str": "${x}", """).getOrElse("")
      val iJson = i.map(x => s""""int": ${x}, """).getOrElse("")
      val json = s"""{$strJson$iJson}"""

      test(json, prog) shouldBe Some((i, str))
    }
  }
}
