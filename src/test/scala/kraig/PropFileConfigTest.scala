package kraig

import cats.free.Free
import cats.instances.all._
import Config._

class PropFileConfigTest extends test.TestBase {
  behavior of "PropFileConfig interpreter"

  def test[A](props: String, prog: Free[Config, A]): Option[A] =
    prog.foldMap(PropFileConfig.interpret(props))

  it should "interpret a string" in {
    val prog = for {
      prop <- getString("prop")
    } yield prop

    forAll { (string: String) =>
      val props = s"""prop=$string"""
      test(props, prog) shouldBe Some(string)
    }
  }

  it should "interpret an int" in {
    val prog = for {
      i <- getInt("prop")
    } yield i

    forAll { (i: Int) =>
      val props = s"""prop=$i"""
      test(props, prog) shouldBe Some(i)
    }
  }

  it should "interpret an optional string and int(when they are absent)" in {
    val prog = for {
      i <- getOptionalInt("int")
      s <- getOptionalString("str")
    } yield (i, s)

    forAll { (str: Option[String], i: Option[Int]) =>
      val strProp = str.map(x => s"""str=${x}""").getOrElse("")
      val iProp = i.map(x => s"""int=${x}""").getOrElse("")
      val props = List(strProp, iProp).mkString("\n")

      test(props, prog) shouldBe Some((i, str))
    }
  }
}
