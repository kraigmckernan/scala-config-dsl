package kraig

import PropName._

import cats.~>

import java.util.Properties
import java.io.StringReader

object PropFileConfig {
  def interpret(props: String): Config ~> Option =
    new (Config ~> Option) {
      val prop = new Properties()
      prop.load(new StringReader(props))

      def apply[A](fa: Config[A]): Option[A] = fa match {
        case GetString(propNames) => {
          Option(
            prop.getProperty(propNames.toDotNotation)
          )
        }
        case GetInt(propNames) => {
          Option(
            prop.getProperty(propNames.toDotNotation).toInt
          )
        }
        case GetOptionalVal(propNames) => {
          Some(
            Option(prop.getProperty(propNames.toDotNotation)).map(ConfigType.deferred)
          )
        }
      }
    }
}
