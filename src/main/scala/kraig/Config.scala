package kraig

import cats.free.Free
import PropName._

sealed trait Config[A]
final case class GetString(propNames: PropNames) extends Config[String]
final case class GetInt(propNames: PropNames) extends Config[Int]
final case class GetOptionalVal(propNames: PropNames) extends Config[Option[ConfigType]]

object Config {
  def getString(propNames: String): Free[Config, String] =
    Free.liftF[Config, String](GetString(fromDotNotation(propNames)))

  def getInt(propNames: String): Free[Config, Int] =
    Free.liftF[Config, Int](GetInt(fromDotNotation(propNames)))

  private def getOptionalVal(propNames: String): Free[Config, Option[ConfigType]] =
    Free.liftF[Config, Option[ConfigType]](GetOptionalVal(fromDotNotation(propNames)))

  def getOptionalString(propNames: String): Free[Config, Option[String]] =
    getOptionalVal(propNames)
      .map(_.flatMap(_.extractString))

  def getOptionalInt(propNames: String): Free[Config, Option[Int]] =
    getOptionalVal(propNames)
      .map(_.flatMap(_.extractInt))
}
