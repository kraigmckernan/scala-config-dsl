package kraig

import cats.{ Id, ~> }
import org.json4s.{ CustomSerializer, DefaultFormats, JValue, JObject, JInt, JString }
import org.json4s.native.JsonMethods.parse

object JsonConfig {
  case object ConfigTypeSerializer extends CustomSerializer[ConfigType](format => (
    {
      case JString(str) => StringType(str)
      case JInt(i) => IntType(i.toInt) // TODO
    }, {
      case StringType(str) => JString(str)
      case IntType(i) => JInt(i)
    }
  ))

  implicit val formats = DefaultFormats ++ List(ConfigTypeSerializer)

  def interpret(json: String): Config ~> Option =
    new (Config ~> Option) {
      val jval: Option[JValue] = Some(parse(json))

      def stepThroughProps(maybeJson: Option[JValue], prop: String): Option[JValue] =
        for {
          maybeMap <- maybeJson
          map <- maybeMap.extractOpt[JObject]
          jval <- map.obj
            .find(_._1 == prop)
            .map(_._2)
        } yield jval

      def apply[A](fa: Config[A]): Option[A] = fa match {
        case GetString(propNames) => {
          propNames
            .foldLeft(jval)(stepThroughProps)
            .flatMap(_.extractOpt[String])
        }
        case GetInt(propNames) => {
          propNames
            .foldLeft(jval)(stepThroughProps)
            .flatMap(_.extractOpt[Int])
        }
        case GetOptionalVal(propNames) => {
          // We always want to return Some() here
          // Failed extraction results in a Some(None) which lets this fail when the whole transform succeeds
          Some(
            propNames
              .foldLeft(jval)(stepThroughProps)
              .flatMap(_.extractOpt[ConfigType])
          )
        }
      }
    }
}
