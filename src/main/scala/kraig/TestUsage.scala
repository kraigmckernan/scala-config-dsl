package kraig

import cats.free.Free
import cats.instances.all._

object TestUsage extends App {
  import Config._

  case class Person(
    firstName: String,
    lastName: String,
    age: Int,
    description: Option[String],
    `null`: Option[String],
    extra: Option[Int]
  )

  val program: Free[Config, Person] = for {
    firstName <- getString("person.name.first")
    lastName <- getString("person.name.last")
    age <- getInt("person.age")
    desc <- getOptionalString("person.description")
    `null` <- getOptionalString("person.null")
    extra <- getOptionalInt("person.integer")
  } yield Person(firstName, lastName, age, desc, `null`, extra)

  val json: String = scala.io.Source.fromResource("person.json").mkString

  val interpreter = JsonConfig.interpret(json)

  val result: Option[Person] = program.foldMap(interpreter)

  println(result)

  val props: String = scala.io.Source.fromResource("person.properties").mkString

  val interpreter2 = PropFileConfig.interpret(props)

  val result2: Option[Person] = program.foldMap(interpreter2)

  println(result2)
}
