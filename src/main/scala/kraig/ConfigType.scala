package kraig

import scala.util.Try

sealed trait ConfigType {
  def extractInt: Option[Int] = None
  def extractString: Option[String] = None
}

final case class IntType(i: Int) extends ConfigType {
  override val extractInt: Option[Int] = Some(i)
}

final case class StringType(str: String) extends ConfigType {
  override val extractString: Option[String] = Some(str)
}

// A class for interpreters that just have a string and can't call it a specific type
//  So we defer to what the program wants
final case class DefferedType(value: String) extends ConfigType {
  override def extractString: Option[String] = Some(value)
  override def extractInt: Option[Int] = Try(value.toInt).toOption
}

object ConfigType {
  def deferred(value: String) = DefferedType(value)
}
