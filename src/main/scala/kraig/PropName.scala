package kraig

object PropName {
  type PropName = String
  type PropNames = Seq[PropName]

  def fromDotNotation(str: String): PropNames = str.split('.')

  implicit class PropNamesOps(propNames: PropNames) {
    def toDotNotation(): String = propNames.mkString(".")
  }
}
