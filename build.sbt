name := "Config DSL"

version := "0.1"

scalaVersion := "2.12.1"

// Dependecies

libraryDependencies += "org.typelevel" %% "cats" % "0.9.0"

libraryDependencies += "org.json4s" %% "json4s-native" % "3.5.0"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.0" % "test"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.2" % "test"
